const { query } = require("express");
const express = require("express");
const Book = require("./Book");
const port = process.env.PORT || 8000;
const app = express();

// it will active request body JSON 
app.use(express.json());

// all end point below this code

//post ./books (create books)
app.post("/api/v1/books", (req, res) => {
    const book = Book.create(req.body);
    res.status(200).json(book);
});

//get list book
app.get("/api/v1/books", (req, res) => {
    const book = Book.list();
    res.status(200).json(book);
});

//get find book by id 
app.get("/api/v1/books/:id", (req, res) => {
    const book = Book.find(req.params.id);
    if(!book) return res.status(404).json(null);
    res.status(201).json(book);
});

//put ./books (update books)
app.put("/api/v1/books/:id", (req, res) => {
    const book = Book.update(req.params.id, req.body);
    if(!book) return res.status(404).json(null);
    res.status(200).json(book);
});

//delete ./books (delete books)
app.delete("/api/v1/books/:id", (req, res) => {
    const book = Book.delete(req.params.id, req.body);
    if(!book) return res.status(404).json(null);
    res.status(204).end();
});

// run the server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
}) ;      
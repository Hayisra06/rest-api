const books = [];
class Book {
    constructor(params){
        this.id = params.id;
        this.title = params.title;
        this.coverImage = params.coverImage;
        this.synopsis = params.synopsis;
        this.publisher = params.publisher;
        this.author = params.author;
        this.price = params.price;
    }
    // list the book 
    static list(){
        return books;
    }
    // find the book by :id
    static find(id){
        const book = books.find((i) => i.id === Number(id));

        if (!book) return null;

        return book;
    }

    //create new book
    static create(params) {
        if (books.length === 0){
            params.id = 1;
        } else {
            params.id = books[books.length - 1].id + 1;
        }

        // create new instance
        const book = new this(params);
        console.log(book)
        // store new book in the books array
        books.push(book);

        //return new book
        return book;
    }

    // update book by id
    static update(id, params){
        const book = this.find(id);

        //if book not exits
        if (!book) return null;

        //update book
        Object.assign(book, params);

        // return the update book
        return book;
    }

    //delete book
    static delete(id){
        const book = this.find(id);
        //if book not exits
        if (!book) return null;
        // delete value by id
        books.splice(books.indexOf(book), 1);
        // return delete book
        return book;
    } 
}
module.exports = Book;